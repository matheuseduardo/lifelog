---
layout: page
title: sobre
permalink: /sobre/
---
{% assign anoatual = "now" | date: "%Y" %}
{% assign mesatual = "now" | date: "%m" %}
{% assign diaatual = "now" | date: "%d" %}
{% assign anonascimento = site.data.author.birth_date | date: "%Y" %}
{% assign mesnascimento = site.data.author.birth_date | date: "%m" %}
{% assign dianascimento = site.data.author.birth_date | date: "%d" %}
{% assign idade = anoatual | minus: anonascimento %}
{% assign idade = idade | minus: 1 %}
{% if mesatual > mesnascimento %}
    {% assign idade = idade | plus: 1 %}
{% elsif mesatual == mesnascimento %}
    {% if diaatual >= dianascimento %}
        {% assign idade = idade | plus: 1 %}
    {% endif %}
{% endif %}
Meu nome é **{{ site.data.author.name }}**. vivo em **[{{ site.data.author.location }}][googlemapslocal]**. Tenho {{ idade }} anos. Sou analista de sistemas, [desenvolvedor][github] / coder por profissão e ofício. Trilheiro, do trekking, do camping, das travessias.. as coisas que **mais curto**. Também [corro][runkeeper] e jogo bola, nas raras horas vagas.

A intenção aqui é colecionar relatos, histórias, memórias.. de vários momentos, locais, cachoeiras, nascer e pores do sol, lua, TUDO!
[gravatar](https://gravatar.com/avatar/{{ site.data.author.email | downcase | md5 }}?s=144)

[github]: https://github.com/matheuseduardo
[runkeeper]: https://runkeeper.com/user/matheuseduardo/profile
[googlemapslocal]: https://google.com/maps?q={{ site.data.author.location | url_encode }}
